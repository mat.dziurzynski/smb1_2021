package pl.edu.pjatk.smb.mdziurzynski.smb1.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import pl.edu.pjatk.smb.mdziurzynski.smb1.databinding.ListElementLocationBinding
import pl.edu.pjatk.smb.mdziurzynski.smb1.model.Location
import pl.edu.pjatk.smb.mdziurzynski.smb1.viewmodel.LocationViewModel

class LocationAdapter(val context: Context, val locationViewModel: LocationViewModel)
    : RecyclerView.Adapter<LocationAdapter.LocationViewHolder>() {

    private var locations = emptyList<Location>()

    inner class LocationViewHolder(val binding: ListElementLocationBinding)
        : RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LocationAdapter.LocationViewHolder {
        val inflater: LayoutInflater = LayoutInflater.from(parent.context)
        val binding = ListElementLocationBinding.inflate(inflater, parent, false)
        return LocationViewHolder(binding)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: LocationViewHolder, position: Int) {
        val currentLocation = locations[position]

        holder.binding.locationName.text = currentLocation.name
        holder.binding.locationDescription.text = currentLocation.description
        holder.binding.locationCoordinates.text = "(${currentLocation.latitude}, ${currentLocation.longitude})"
        holder.binding.locationRadius.text = "${currentLocation.radius}$DISTANCE_UNIT"
    }

    override fun getItemCount(): Int = locations.size

    internal fun setLocations(locations: List<Location>) {
        this.locations = locations
        notifyDataSetChanged()
    }

    fun updateLocation(position: Int, name: String, description: String, radius: Float, latitude: Float, longitude: Float) {
        val currentLocation = locations[position]

        currentLocation.name = name
        currentLocation.description = description
        currentLocation.radius = radius
        currentLocation.latitude = latitude
        currentLocation.longitude = longitude

        locationViewModel.update(currentLocation)
    }

    companion object {
        private const val DISTANCE_UNIT = "m"
    }
}