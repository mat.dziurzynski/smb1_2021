package pl.edu.pjatk.smb.mdziurzynski.smb1.utils

import android.content.Context
import android.app.NotificationManager
import androidx.core.app.NotificationCompat
import pl.edu.pjatk.smb.mdziurzynski.smb1.R
import kotlin.math.roundToInt

fun NotificationManager.sendGeofenceNotification(context: Context, title: String, text: String) {
    val builder = NotificationCompat.Builder(context, context.getString(R.string.channel_id))
        .setContentTitle(title)
        .setContentText(text)
        .setSmallIcon(R.mipmap.ic_launcher)
        .setPriority(NotificationCompat.PRIORITY_DEFAULT)
        .setAutoCancel(true)
        .build()

    this.notify((Math.random() * 1000 * 1000).roundToInt(), builder)
}