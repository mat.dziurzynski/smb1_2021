package pl.edu.pjatk.smb.mdziurzynski.smb1

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProvider
import com.google.android.gms.location.LocationServices
import pl.edu.pjatk.smb.mdziurzynski.smb1.databinding.ActivityLocationAddBinding
import pl.edu.pjatk.smb.mdziurzynski.smb1.model.Location
import pl.edu.pjatk.smb.mdziurzynski.smb1.viewmodel.LocationViewModel
import pl.edu.pjatk.smb.mdziurzynski.smb1.viewmodel.ProductViewModel

class LocationAddActivity : AppCompatActivity() {

    private lateinit var binding: ActivityLocationAddBinding
    private lateinit var locationViewModel: LocationViewModel

    private var latitude = 0.0F
    private var longitude = 0.0F

    override fun onCreate(savedInstanceState: Bundle?) {
        establishTheme()
        super.onCreate(savedInstanceState)
        binding = ActivityLocationAddBinding.inflate(layoutInflater)
        setContentView(binding.root)

        locationViewModel = ViewModelProvider(this)[LocationViewModel::class.java]
    }

    @SuppressLint("MissingPermission", "SetTextI18n")
    override fun onResume() {
        super.onResume()

        LocationServices.getFusedLocationProviderClient(this).lastLocation
            .addOnSuccessListener {
                latitude = it.latitude.toFloat()
                longitude = it.longitude.toFloat()

                this.binding.textCoordinates.text = "($latitude, $longitude)"
            }
    }

    private fun establishTheme() {
        // Import values from shared preferences
        val sharedPreferences = getSharedPreferences("SMBSharedPreferences", Context.MODE_PRIVATE)
        val theme = sharedPreferences.getInt("Theme", 0)
        val font = sharedPreferences.getInt("Font", 0)

        // Establish color palette and font
        if (theme == 0 && font == 0) {
            setTheme(R.style.Ocean_Helvetica_SMB1)
        } else if (theme == 0 && font == 1) {
            setTheme(R.style.Ocean_CourierNew_SMB1)
        } else if (theme == 1 && font == 0) {
            setTheme(R.style.Desert_Helvetica_SMB1)
        } else {
            setTheme(R.style.Desert_CourierNew_SMB1)
        }
    }

    fun onButtonPressed(view: View) {
        val locationName: String = binding.locationEditName.text.toString()
        val locationDescription: String = binding.locationEditDescription.text.toString()
        val locationRadius: Float = binding.locationEditRadius.text.toString().toFloat()

        val locationToAdd = Location(
            name = locationName,
            description = locationDescription,
            radius = locationRadius,
            latitude = latitude,
            longitude = longitude
        )

        locationViewModel.insert(locationToAdd)

        val intent = Intent(this, MapActivity::class.java)
        startActivity(intent)
    }
}