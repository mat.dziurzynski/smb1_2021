package pl.edu.pjatk.smb.mdziurzynski.smb1.repo

import androidx.lifecycle.LiveData
import pl.edu.pjatk.smb.mdziurzynski.smb1.dao.LocationDao
import pl.edu.pjatk.smb.mdziurzynski.smb1.model.Location

class LocationRepository(private val locationDao: LocationDao) {
    val allLocations: LiveData<List<Location>> = locationDao.getLocations()

    fun insert(location: Location) {
        locationDao.insert(location)
    }

    fun update(location: Location) {
        locationDao.update(location)
    }

    fun delete(location: Location) {
        locationDao.delete(location)
    }

    fun deleteLocations() {
        locationDao.deleteLocations()
    }
}