package pl.edu.pjatk.smb.mdziurzynski.smb1.widget

import android.app.PendingIntent
import android.appwidget.AppWidgetManager
import android.appwidget.AppWidgetProvider
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.media.MediaPlayer
import android.net.Uri
import android.util.Log
import android.widget.RemoteViews
import pl.edu.pjatk.smb.mdziurzynski.smb1.R

/**
 * Implementation of App Widget functionality.
 */
class MultimediaWidget : AppWidgetProvider() {

    private lateinit var mediaPlayer: MediaPlayer

    override fun onUpdate(
        context: Context,
        appWidgetManager: AppWidgetManager,
        appWidgetIds: IntArray,
    ) {
        // There may be multiple widgets active, so update all of them
        for (appWidgetId in appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId)
        }
    }

    override fun onEnabled(context: Context) {
        Log.i("MultimediaWidget", "Widget created.")
    }

    override fun onDisabled(context: Context) {
        Log.i("MultimediaWidget", "Widget destroyed.")
    }

    override fun onReceive(context: Context?, intent: Intent?) {
        super.onReceive(context, intent)

        // Construct the RemoteViews and AppWidgetManager objects
        val views = RemoteViews(context?.packageName, R.layout.multimedia_widget)
        val appWidgetManager = AppWidgetManager.getInstance(context)

        if (intent?.action.equals("IMAGE_UPDATE_ACTION")) {
            // Get widgetId from intent
            val appWidgetId = intent?.getIntExtra("appWidgetId", 0)

            // Change image to random one
            val images = listOf(
                R.drawable.nature_001,
                R.drawable.nature_002,
                R.drawable.nature_003,
                R.drawable.nature_004,
                R.drawable.nature_005,
                R.drawable.nature_006
            )
            views.setImageViewResource(R.id.display_image, images.random())

            // Instruct the widget manager to update the widget
            appWidgetManager.updateAppWidget(appWidgetId!!, views)
        } else if (intent?.action.equals("AUDIO_PLAY_PAUSE")) {
            // Get widgetId from intent
            val appWidgetId = intent?.getIntExtra("appWidgetId", 0)

            val mediaPlayer = MediaPlayer.create(
                    context,
                    R.raw.nge_intro
            )
            mediaPlayer.start()

            val pauseText = context?.getString(R.string.widget_button_pause)
            views.setTextViewText(R.id.button_play_pause, pauseText)

            // Instruct the widget manager to update the widget
            appWidgetManager.updateAppWidget(appWidgetId!!, views)
        } else if (intent?.action.equals("AUDIO_STOP")) {
            // TODO: Add logic to stop mediaPlayer
        } else if (intent?.action.equals("AUDIO_NEXT")) {
            // TODO: Add logic to play next song
        }
    }
}

internal fun updateAppWidget(
    context: Context,
    appWidgetManager: AppWidgetManager,
    appWidgetId: Int,
) {
    // Construct the RemoteViews object
    val views = RemoteViews(context.packageName, R.layout.multimedia_widget)

    // Set header text
    val widgetText = context.getString(R.string.widget_header)
    views.setTextViewText(R.id.widget_header, widgetText)

    // Add intent to hyperlink text
    val uriIntent = Intent(Intent.ACTION_VIEW)
    uriIntent.data = Uri.parse("https://www.pja.edu.pl/")

    val uriPendingIntent = PendingIntent.getActivity(
        context,
        0,
        uriIntent,
        PendingIntent.FLAG_UPDATE_CURRENT
    )
    views.setOnClickPendingIntent(R.id.section_link, uriPendingIntent)

    // Add intent to display image
    val imageIntent = Intent("IMAGE_UPDATE_ACTION")
    imageIntent.component = ComponentName(context, MultimediaWidget::class.java)
    imageIntent.putExtra("appWidgetId", appWidgetId)

    val imagePendingIntent = PendingIntent.getBroadcast(
        context,
        0,
        imageIntent,
        PendingIntent.FLAG_UPDATE_CURRENT
    )
    views.setOnClickPendingIntent(R.id.display_image_button, imagePendingIntent)

    // Add intent to audio play/pause button
    val playPauseIntent = Intent("AUDIO_PLAY_PAUSE")
    playPauseIntent.component = ComponentName(context, MultimediaWidget::class.java)
    playPauseIntent.putExtra("appWidgetId", appWidgetId)

    val playPausePendingIntent = PendingIntent.getBroadcast(
        context,
        0,
        playPauseIntent,
        PendingIntent.FLAG_UPDATE_CURRENT
    )
    views.setOnClickPendingIntent(R.id.button_play_pause, playPausePendingIntent)

    // Add intent to audio stop button
    val stopIntent = Intent("AUDIO_STOP")
    stopIntent.component = ComponentName(context, MultimediaWidget::class.java)
    stopIntent.putExtra("appWidgetId", appWidgetId)

    val stopPendingIntent = PendingIntent.getBroadcast(
        context,
        0,
        stopIntent,
        PendingIntent.FLAG_UPDATE_CURRENT
    )
    views.setOnClickPendingIntent(R.id.button_stop, stopPendingIntent)

    // Add intent to audio next button
    val nextIntent = Intent("AUDIO_NEXT")
    nextIntent.component = ComponentName(context, MultimediaWidget::class.java)
    nextIntent.putExtra("appWidgetId", appWidgetId)

    val nextPendingIntent = PendingIntent.getBroadcast(
        context,
        0,
        nextIntent,
        PendingIntent.FLAG_UPDATE_CURRENT
    )
    views.setOnClickPendingIntent(R.id.button_next, nextPendingIntent)

    // Instruct the widget manager to update the widget
    appWidgetManager.updateAppWidget(appWidgetId, views)
}