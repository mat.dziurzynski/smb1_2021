package pl.edu.pjatk.smb.mdziurzynski.smb1

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import pl.edu.pjatk.smb.mdziurzynski.smb1.adapter.LocationAdapter
import pl.edu.pjatk.smb.mdziurzynski.smb1.databinding.ActivityLocationListBinding
import pl.edu.pjatk.smb.mdziurzynski.smb1.viewmodel.LocationViewModel

class LocationListActivity : AppCompatActivity() {

    private lateinit var binding: ActivityLocationListBinding
    private lateinit var locationViewModel: LocationViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        establishTheme()
        super.onCreate(savedInstanceState)
        binding = ActivityLocationListBinding.inflate(layoutInflater)
        setContentView(binding.favouriteLocations)

        binding.locationList.layoutManager = LinearLayoutManager(this)
        binding.locationList.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))

        locationViewModel = ViewModelProvider(this)[LocationViewModel::class.java]
        locationViewModel.allLocations.observe(this, Observer { locations ->
            locations?.let {
                (binding.locationList.adapter as LocationAdapter).setLocations(it)
            }
        })

        binding.locationList.adapter = LocationAdapter(this, locationViewModel)
    }

    private fun establishTheme() {
        // Import values from shared preferences
        val sharedPreferences = getSharedPreferences("SMBSharedPreferences", Context.MODE_PRIVATE)
        val theme = sharedPreferences.getInt("Theme", 0)
        val font = sharedPreferences.getInt("Font", 0)

        // Establish color palette and font
        if (theme == 0 && font == 0) {
            setTheme(R.style.Ocean_Helvetica_SMB1)
        } else if (theme == 0 && font == 1) {
            setTheme(R.style.Ocean_CourierNew_SMB1)
        } else if (theme == 1 && font == 0) {
            setTheme(R.style.Desert_Helvetica_SMB1)
        } else {
            setTheme(R.style.Desert_CourierNew_SMB1)
        }
    }

    fun onDeleteAllLocations(view: View) {
        locationViewModel.deleteLocations()
    }
}