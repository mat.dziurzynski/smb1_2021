package pl.edu.pjatk.smb.mdziurzynski.smb1

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.RadioButton
import android.widget.RadioGroup
import androidx.appcompat.app.AppCompatActivity

class SettingsActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        establishTheme()
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_options)
    }

    private fun establishTheme() {
        // Import values from shared preferences
        val sharedPreferences = getSharedPreferences("SMBSharedPreferences", Context.MODE_PRIVATE)
        val theme = sharedPreferences.getInt("Theme", 0)
        val font = sharedPreferences.getInt("Font", 0)

        // Establish color palette and font
        if (theme == 0 && font == 0) {
            setTheme(R.style.Ocean_Helvetica_SMB1)
        } else if (theme == 0 && font == 1) {
            setTheme(R.style.Ocean_CourierNew_SMB1)
        } else if (theme == 1 && font == 0) {
            setTheme(R.style.Desert_Helvetica_SMB1)
        } else {
            setTheme(R.style.Desert_CourierNew_SMB1)
        }
    }

    fun onSaveChanges(view: View) {
        // Extract theme index
        val themeGroup = findViewById<RadioGroup>(R.id.group_theme)
        val chosenThemeId = themeGroup.checkedRadioButtonId
        val themeButton = themeGroup.findViewById<RadioButton>(chosenThemeId)
        val chosenThemeIndex = themeGroup.indexOfChild(themeButton)

        // Extract font index
        val fontGroup = findViewById<RadioGroup>(R.id.group_font)
        val chosenFontId = fontGroup.checkedRadioButtonId
        val fontButton = fontGroup.findViewById<RadioButton>(chosenFontId)
        val chosenFontIndex = fontGroup.indexOfChild(fontButton)

        // Set up shared preferences
        val sharedPreferences = getSharedPreferences("SMBSharedPreferences", Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()

        // Update data in shared preferences
        editor.putInt("Theme", chosenThemeIndex)
        editor.putInt("Font", chosenFontIndex)
        editor.apply()

        // Necessary to apply changes dynamically
        recreate()
    }

    fun onNavigateToCart(view: View) {
        val intent = Intent(this, CartActivity::class.java)
        startActivity(intent)
    }

    fun onNavigateToHome(view: View) {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
    }
}