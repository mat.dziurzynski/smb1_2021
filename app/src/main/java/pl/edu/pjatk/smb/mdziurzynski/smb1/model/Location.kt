package pl.edu.pjatk.smb.mdziurzynski.smb1.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "location_table")
data class Location(@PrimaryKey(autoGenerate = true) var id: Long = 0,
                    var name: String,
                    var description: String,
                    var radius: Float,
                    var latitude: Float,
                    var longitude: Float) {
}