package pl.edu.pjatk.smb.mdziurzynski.smb1

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProvider
import pl.edu.pjatk.smb.mdziurzynski.smb1.databinding.ActivityProductBinding
import pl.edu.pjatk.smb.mdziurzynski.smb1.model.Product
import pl.edu.pjatk.smb.mdziurzynski.smb1.viewmodel.ProductViewModel

class ProductActivity : AppCompatActivity() {

    private lateinit var binding: ActivityProductBinding
    private lateinit var productViewModel: ProductViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        establishTheme()
        super.onCreate(savedInstanceState)
        binding = ActivityProductBinding.inflate(layoutInflater)
        setContentView(binding.product)

        productViewModel = ViewModelProvider(this)[ProductViewModel::class.java]

        if (intent.getStringExtra("mode") == "edit") {
            binding.textProduct.text = getString(R.string.product_header_edit)
            binding.productEditName.setText(intent.getStringExtra("name"))
            binding.productEditPrice.setText(intent.getFloatExtra("price", 0.0F).toString())
            binding.productEditAmount.setText(intent.getFloatExtra("amount", 1.0F).toString())
            binding.checkbox.isChecked = intent.getBooleanExtra("isBought", false)
            binding.checkbox.setText(R.string.product_checkbox_checked)
        } else {
            binding.textProduct.text = getString(R.string.product_header_add)
            binding.productEditName.setText("")
            binding.productEditPrice.setText("")
            binding.productEditAmount.setText("")
            binding.checkbox.isChecked = false
            binding.checkbox.setText(R.string.product_checkbox_unchecked)
        }
    }

    private fun establishTheme() {
        // Import values from shared preferences
        val sharedPreferences = getSharedPreferences("SMBSharedPreferences", Context.MODE_PRIVATE)
        val theme = sharedPreferences.getInt("Theme", 0)
        val font = sharedPreferences.getInt("Font", 0)

        // Establish color palette and font
        if (theme == 0 && font == 0) {
            setTheme(R.style.Ocean_Helvetica_SMB1)
        } else if (theme == 0 && font == 1) {
            setTheme(R.style.Ocean_CourierNew_SMB1)
        } else if (theme == 1 && font == 0) {
            setTheme(R.style.Desert_Helvetica_SMB1)
        } else {
            setTheme(R.style.Desert_CourierNew_SMB1)
        }
    }

    fun onButtonPressed(view: View) {
        val productName: String = binding.productEditName.text.toString()
        val productPrice: Float = binding.productEditPrice.text.toString().toFloat()
        val productAmount: Float = binding.productEditAmount.text.toString().toFloat()
        val isProductBought: Boolean = binding.checkbox.isChecked

        // Adding new product if requested
        if (intent.getStringExtra("mode") == "add") { // TODO: change to act on "mode" intent var
            val productToAdd = Product(
                name = productName,
                price = productPrice,
                amount = productAmount,
                isBought = isProductBought)

            productViewModel.insert(productToAdd)
        }

        // Updating existing product if requested
        if (intent.getStringExtra("mode") == "edit") {
            val productToUpdate = Product(
                id = intent.getLongExtra("productId", 0),
                name = productName,
                price = productPrice,
                amount = productAmount,
                isBought = isProductBought)

            productViewModel.update(productToUpdate)
        }

        val intent = Intent(this, CartActivity::class.java)
        startActivity(intent)
    }

    fun onToggleChecked(view: View) {
        val isChecked: Boolean = binding.checkbox.isChecked

        if (isChecked) {
            binding.checkbox.text = getString(R.string.product_checkbox_checked)
        } else {
            binding.checkbox.text = getString(R.string.product_checkbox_unchecked)
        }
    }
}