package pl.edu.pjatk.smb.mdziurzynski.smb1.db

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.os.Build
import androidx.annotation.RequiresApi

@RequiresApi(Build.VERSION_CODES.P)
class DatabaseHelper(context: Context?): SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {

    override fun onCreate(db: SQLiteDatabase?) {
        db?.execSQL(CREATE_PRODUCTS_TABLE)
        db?.execSQL(CREATE_LOCATIONS_TABLE)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        // TODO("Not yet implemented")
    }

    companion object {
        // General
        private const val DATABASE_VERSION = 1
        private const val DATABASE_NAME = "ShoppingCartDatabase"
        private const val TABLE_PRODUCTS = "ProductsTable"
        private const val TABLE_LOCATIONS = "LocationsTable"

        // Keys
        private const val KEY_ID = "id"
        private const val KEY_NAME = "name"
        private const val KEY_PRICE = "price"
        private const val KEY_AMOUNT = "amount"
        private const val KEY_BOUGHT = "isBought"
        private const val KEY_DESCRIPTION = "description"
        private const val KEY_RADIUS = "radius"
        private const val KEY_LATITUDE = "latitude"
        private const val KEY_LONGITUDE = "longitude"

        // Queries
        private const val CREATE_PRODUCTS_TABLE = "CREATE TABLE " + TABLE_PRODUCTS + " (" +
                KEY_ID + " INTEGER PRIMARY KEY," +
                KEY_NAME + " TEXT," +
                KEY_PRICE + " REAL," +
                KEY_AMOUNT + " REAL," +
                KEY_BOUGHT + " INTEGER);"

        private const val CREATE_LOCATIONS_TABLE = "CREATE TABLE " + TABLE_LOCATIONS + " (" +
                KEY_ID + " INTEGER PRIMARY KEY," +
                KEY_NAME + " TEXT," +
                KEY_DESCRIPTION + " TEXT," +
                KEY_RADIUS + " REAL," +
                KEY_LATITUDE + " REAL," +
                KEY_LONGITUDE + " REAL);"
    }
}