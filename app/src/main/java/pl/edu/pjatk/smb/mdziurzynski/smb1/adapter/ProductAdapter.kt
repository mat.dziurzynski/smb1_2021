package pl.edu.pjatk.smb.mdziurzynski.smb1.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import pl.edu.pjatk.smb.mdziurzynski.smb1.ProductActivity import pl.edu.pjatk.smb.mdziurzynski.smb1.databinding.ListElementBinding
import pl.edu.pjatk.smb.mdziurzynski.smb1.model.Product
import pl.edu.pjatk.smb.mdziurzynski.smb1.viewmodel.ProductViewModel

class ProductAdapter(val context: Context, val productViewModel: ProductViewModel)
    : RecyclerView.Adapter<ProductAdapter.ProductViewHolder>() {

    private var products = emptyList<Product>()

    inner class ProductViewHolder(val binding: ListElementBinding)
        : RecyclerView.ViewHolder(binding.root) {
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {
        val inflater: LayoutInflater = LayoutInflater.from(parent.context)
        val binding = ListElementBinding.inflate(inflater, parent, false)
        return ProductViewHolder(binding)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {
        val currentProduct = products[position]

        holder.binding.productName.text = currentProduct.name
        holder.binding.productPrice.text = currentProduct.price.toString() + " " + DEFAULT_CURRENCY // TODO: Change display format to double digit precision
        holder.binding.productAmount.text = TIMES_OPERATOR + currentProduct.amount.toString()
        holder.binding.checkbox.isChecked = currentProduct.isBought

        val intent = Intent(context, ProductActivity::class.java).apply {
            putExtra("mode", "edit")
            putExtra("productId", currentProduct.id)
            putExtra("name", currentProduct.name)
            putExtra("price", currentProduct.price)
            putExtra("amount", currentProduct.amount)
            putExtra("isBought", currentProduct.isBought)
        }

        holder.binding.productName.setOnClickListener {
            context.startActivity(intent)
        }
    }

    override fun getItemCount(): Int = products.size

    internal fun setProducts(products: List<Product>) {
        this.products = products
        notifyDataSetChanged()
    }

    fun updateProduct(position: Int, name: String, price: Float, amount: Float, isBought: Boolean) {
        val currentProduct = products[position]

        currentProduct.name = name
        currentProduct.price = price
        currentProduct.amount = amount
        currentProduct.isBought = isBought

        productViewModel.update(currentProduct)
    }

    companion object {
        private const val TIMES_OPERATOR = "x"
        private const val DEFAULT_CURRENCY = "zł"
    }
}