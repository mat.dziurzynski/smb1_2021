package pl.edu.pjatk.smb.mdziurzynski.smb1

import android.Manifest
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.app.NotificationManagerCompat
import pl.edu.pjatk.smb.mdziurzynski.smb1.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    @RequiresApi(Build.VERSION_CODES.Q)
    override fun onCreate(savedInstanceState: Bundle?) {
        establishTheme()
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        acceptNecessaryPermissions()
        createNotificationChannel()
    }

    @RequiresApi(Build.VERSION_CODES.Q)
    private fun acceptNecessaryPermissions() {
        val permissions = arrayOf(
            Manifest.permission.ACCESS_BACKGROUND_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION
        )

        val requestCode = 1

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
            && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
            && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_BACKGROUND_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(permissions, requestCode)
            }
    }

    private fun createNotificationChannel() {
        // for SDK >= 26
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            return
        }

        val notificationChannel = NotificationChannel(
            getString(R.string.channel_id),
            getString(R.string.channel_name),
            NotificationManager.IMPORTANCE_DEFAULT
        )

        notificationChannel.description = getString(R.string.channel_description)

        val notificationManager = NotificationManagerCompat.from(this)
        notificationManager.createNotificationChannel(notificationChannel)
    }

    private fun establishTheme() {
        // Import values from shared preferences
        val sharedPreferences = getSharedPreferences("SMBSharedPreferences", Context.MODE_PRIVATE)
        val theme = sharedPreferences.getInt("Theme", 0)
        val font = sharedPreferences.getInt("Font", 0)

        // Establish color palette and font
        if (theme == 0 && font == 0) {
            setTheme(R.style.Ocean_Helvetica_SMB1)
        } else if (theme == 0 && font == 1) {
            setTheme(R.style.Ocean_CourierNew_SMB1)
        } else if (theme == 1 && font == 0) {
            setTheme(R.style.Desert_Helvetica_SMB1)
        } else {
            setTheme(R.style.Desert_CourierNew_SMB1)
        }
    }

    fun onNavigateToCart(view: View) {
        val intent = Intent(this, CartActivity::class.java)
        startActivity(intent)
    }

    fun onNavigateToMap(view: View) {
        val intent = Intent(this, MapActivity::class.java)
        startActivity(intent)
    }

    fun onNavigateToProduct(view: View) {
        val intent = Intent(this, ProductActivity::class.java)
        startActivity(intent)
    }

    fun onNavigateToSettings(view: View) {
        val intent = Intent(this, SettingsActivity::class.java)
        startActivity(intent)
    }
}