package pl.edu.pjatk.smb.mdziurzynski.smb1.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "product_table")
data class Product(@PrimaryKey(autoGenerate = true) var id: Long = 0,
                   var name: String,
                   var price: Float,
                   var amount: Float,
                   var isBought: Boolean) {

}