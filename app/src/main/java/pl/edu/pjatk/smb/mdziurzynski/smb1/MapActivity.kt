package pl.edu.pjatk.smb.mdziurzynski.smb1

import android.annotation.SuppressLint
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.android.gms.location.Geofence
import com.google.android.gms.location.GeofencingClient
import com.google.android.gms.location.GeofencingRequest
import com.google.android.gms.location.LocationServices

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.CircleOptions
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import pl.edu.pjatk.smb.mdziurzynski.smb1.databinding.ActivityMapBinding
import pl.edu.pjatk.smb.mdziurzynski.smb1.receiver.GeofenceReceiver
import pl.edu.pjatk.smb.mdziurzynski.smb1.viewmodel.LocationViewModel

class MapActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap
    private lateinit var binding: ActivityMapBinding
    private lateinit var locationViewModel: LocationViewModel
    private lateinit var geoClient: GeofencingClient

    override fun onCreate(savedInstanceState: Bundle?) {
        establishTheme()
        super.onCreate(savedInstanceState)
        binding = ActivityMapBinding.inflate(layoutInflater)
        setContentView(binding.map)

        geoClient = LocationServices.getGeofencingClient(this)

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map_fragment) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    @SuppressLint("MissingPermission")
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        val rotunda = LatLng(52.230051, 21.011907)

        var geofenceId = 0
        locationViewModel = ViewModelProvider(this)[LocationViewModel::class.java]
        locationViewModel.allLocations.observe(this, Observer { locations ->
            locations?.forEach {

                // Add marker to map
                val markerOptions = MarkerOptions()
                val coordinates = LatLng(it.latitude.toDouble(), it.longitude.toDouble())
                markerOptions.position(coordinates)
                markerOptions.title(it.name)
                markerOptions.snippet(it.description)
                mMap.addMarker(markerOptions)

                // Add circle with adequate radius to marker location
                val circleOptions = CircleOptions()
                circleOptions.center(coordinates)
                circleOptions.radius(it.radius.toDouble())
                circleOptions.fillColor(Color.parseColor("#8090E0EF"))
                circleOptions.strokeColor(Color.parseColor("#90E0EF"))
                circleOptions.strokeWidth(10.0F)
                mMap.addCircle(circleOptions)

                // Build geofence from marker data
                val geofence = Geofence.Builder().setRequestId("Geo${geofenceId++} - ${it.name}")
                    .setCircularRegion(
                        it.latitude.toDouble(),
                        it.longitude.toDouble(),
                        it.radius
                    )
                    .setExpirationDuration(60 * 60 * 1000)
                    .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER or Geofence.GEOFENCE_TRANSITION_EXIT)
                    .build()

                // Add geofence to map and listeners to client
                geoClient.addGeofences(getGeofencingRequest(geofence), getGeofencePendingIntent())
                    .addOnSuccessListener {
                        Toast.makeText(
                            this,
                            "Geofence został dodany.",
                            Toast.LENGTH_SHORT)
                            .show()
                    }
                    .addOnFailureListener { it ->
                        Log.e("ERROR", it.toString())
                        Toast.makeText(
                            this,
                            "Geofence nie został dodany.",
                            Toast.LENGTH_SHORT)
                            .show()
                    }
            }
        })

        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(rotunda, 15.0f))
        mMap.isMyLocationEnabled = true
    }

    private fun getGeofencingRequest(geofence: Geofence): GeofencingRequest{
        return GeofencingRequest.Builder()
            .setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER)
            .addGeofence(geofence)
            .build()
    }
    @SuppressLint("UnspecifiedImmutableFlag")
    private fun getGeofencePendingIntent(): PendingIntent {
        return PendingIntent.getBroadcast(
            this, 0,
            Intent(this, GeofenceReceiver::class.java),
            PendingIntent.FLAG_UPDATE_CURRENT)
    }

    private fun establishTheme() {
        // Import values from shared preferences
        val sharedPreferences = getSharedPreferences("SMBSharedPreferences", Context.MODE_PRIVATE)
        val theme = sharedPreferences.getInt("Theme", 0)
        val font = sharedPreferences.getInt("Font", 0)

        // Establish color palette and font
        if (theme == 0 && font == 0) {
            setTheme(R.style.Ocean_Helvetica_SMB1)
        } else if (theme == 0 && font == 1) {
            setTheme(R.style.Ocean_CourierNew_SMB1)
        } else if (theme == 1 && font == 0) {
            setTheme(R.style.Desert_Helvetica_SMB1)
        } else {
            setTheme(R.style.Desert_CourierNew_SMB1)
        }
    }

    fun onNavigateToLocationAdd(view: View) {
        val intent = Intent(this, LocationAddActivity::class.java)
        startActivity(intent)
    }

    fun onNavigateToLocationList(view: View) {
        val intent = Intent(this, LocationListActivity::class.java)
        startActivity(intent)
    }
}