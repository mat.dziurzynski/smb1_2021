package pl.edu.pjatk.smb.mdziurzynski.smb1.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import pl.edu.pjatk.smb.mdziurzynski.smb1.db.DatabaseHandler
import pl.edu.pjatk.smb.mdziurzynski.smb1.model.Location
import pl.edu.pjatk.smb.mdziurzynski.smb1.repo.LocationRepository

class LocationViewModel(application: Application) : AndroidViewModel(application) {

    private val repository: LocationRepository
    val allLocations: LiveData<List<Location>>

    init {
        val locationDao = DatabaseHandler.getDatabase(application).locationDao()
        repository = LocationRepository(locationDao)
        allLocations = repository.allLocations
    }

    fun insert(location: Location) = repository.insert(location)
    fun update(location: Location) = repository.update(location)
    fun delete(location: Location) = repository.delete(location)
    fun deleteLocations() = repository.deleteLocations()
}