package pl.edu.pjatk.smb.mdziurzynski.smb1.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import pl.edu.pjatk.smb.mdziurzynski.smb1.db.DatabaseHandler
import pl.edu.pjatk.smb.mdziurzynski.smb1.model.Product
import pl.edu.pjatk.smb.mdziurzynski.smb1.repo.ProductRepository

class ProductViewModel(application: Application) : AndroidViewModel(application) {

    private val repository: ProductRepository
    val allProducts: LiveData<List<Product>>

    init {
        val productDao = DatabaseHandler.getDatabase(application).productDao()
        repository = ProductRepository(productDao)
        allProducts = repository.allProducts
    }

    fun insert(product: Product) = repository.insert(product)
    fun update(product: Product) = repository.update(product)
    fun delete(product: Product) = repository.delete(product)
    fun deleteProducts() = repository.deleteProducts()
}