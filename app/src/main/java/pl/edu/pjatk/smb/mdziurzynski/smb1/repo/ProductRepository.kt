package pl.edu.pjatk.smb.mdziurzynski.smb1.repo

import androidx.lifecycle.LiveData
import pl.edu.pjatk.smb.mdziurzynski.smb1.dao.ProductDao
import pl.edu.pjatk.smb.mdziurzynski.smb1.model.Product

class ProductRepository(private val productDao: ProductDao) {
    val allProducts: LiveData<List<Product>> = productDao.getProducts()

    fun insert(product: Product) {
        productDao.insert(product)
    }

    fun update(product: Product) {
        productDao.update(product)
    }

    fun delete(product: Product) {
        productDao.delete(product)
    }

    fun deleteProducts() {
        productDao.deleteProducts()
    }
}