package pl.edu.pjatk.smb.mdziurzynski.smb1.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import pl.edu.pjatk.smb.mdziurzynski.smb1.model.Product

@Dao
interface ProductDao {

    @Query(QUERY_GET_PRODUCTS)
    fun getProducts(): LiveData<List<Product>>

    @Insert
    fun insert(product: Product)

    @Update
    fun update(product: Product)

    @Delete
    fun delete(product: Product)

    @Query(QUERY_DELETE_BY_ID)
    fun deleteById(foundId: Long)

    @Query(QUERY_DELETE_PRODUCTS)
    fun deleteProducts()

    companion object {
        private const val QUERY_GET_PRODUCTS = "SELECT * FROM product_table;"
        private const val QUERY_DELETE_BY_ID = "DELETE FROM product_table WHERE product_table.id = :foundId"
        private const val QUERY_DELETE_PRODUCTS = "DELETE FROM product_table;"
    }
}