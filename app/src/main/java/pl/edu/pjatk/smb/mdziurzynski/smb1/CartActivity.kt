package pl.edu.pjatk.smb.mdziurzynski.smb1

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import pl.edu.pjatk.smb.mdziurzynski.smb1.adapter.ProductAdapter
import pl.edu.pjatk.smb.mdziurzynski.smb1.databinding.ActivityCartBinding
import pl.edu.pjatk.smb.mdziurzynski.smb1.model.Product
import pl.edu.pjatk.smb.mdziurzynski.smb1.viewmodel.ProductViewModel

class CartActivity : AppCompatActivity() {

    private lateinit var binding: ActivityCartBinding
    private lateinit var productViewModel: ProductViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        establishTheme()
        super.onCreate(savedInstanceState)
        binding = ActivityCartBinding.inflate(layoutInflater)
        setContentView(binding.cart)

        binding.shoppingList.layoutManager = LinearLayoutManager(this)
        binding.shoppingList.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))

        productViewModel = ViewModelProvider(this)[ProductViewModel::class.java]
        productViewModel.allProducts.observe(this, Observer { products ->
            products?.let {
                (binding.shoppingList.adapter as ProductAdapter).setProducts(it)
            }
        })

        binding.shoppingList.adapter = ProductAdapter(this, productViewModel)

        // Checking if a new product should be added
        if (this.intent.getBooleanExtra("newProduct", false)) {
            val name = this.intent.getStringExtra("name").toString()
            val price = this.intent.getFloatExtra("price", 0.0F)
            val amount = this.intent.getFloatExtra("amount", 1.0F)
            val isBought = this.intent.getBooleanExtra("isBought", false)

            val productToAdd = Product(
                name = name,
                price = price,
                amount = amount,
                isBought = isBought)

            productViewModel.insert(productToAdd)
        }
    }

    private fun establishTheme() {
        // Import values from shared preferences
        val sharedPreferences = getSharedPreferences("SMBSharedPreferences", Context.MODE_PRIVATE)
        val theme = sharedPreferences.getInt("Theme", 0)
        val font = sharedPreferences.getInt("Font", 0)

        // Establish color palette and font
        if (theme == 0 && font == 0) {
            setTheme(R.style.Ocean_Helvetica_SMB1)
        } else if (theme == 0 && font == 1) {
            setTheme(R.style.Ocean_CourierNew_SMB1)
        } else if (theme == 1 && font == 0) {
            setTheme(R.style.Desert_Helvetica_SMB1)
        } else {
            setTheme(R.style.Desert_CourierNew_SMB1)
        }
    }

    fun onDeleteAllProducts(view: View) {
        productViewModel.deleteProducts()
    }

    fun onNavigateToHome(view: View) {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
    }

    fun onNavigateToProduct(view: View) {
        val intent = Intent(this, ProductActivity::class.java).apply {
            putExtra("mode", "add")
        }
        startActivity(intent)
    }

    fun onNavigateToSettings(view: View) {
        val intent = Intent(this, SettingsActivity::class.java)
        startActivity(intent)
    }

    fun onToggleCheckedInCart(view: View) {
        println("Checked: #" + view.id)
    }
}