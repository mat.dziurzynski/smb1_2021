package pl.edu.pjatk.smb.mdziurzynski.smb1.receiver

import android.app.NotificationManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import com.google.android.gms.location.Geofence
import com.google.android.gms.location.GeofencingEvent
import pl.edu.pjatk.smb.mdziurzynski.smb1.utils.sendGeofenceNotification
import kotlin.math.roundToInt

class GeofenceReceiver : BroadcastReceiver() {
    @RequiresApi(Build.VERSION_CODES.O)
    override fun onReceive(context: Context, intent: Intent) {
        val geoEvent = GeofencingEvent.fromIntent(intent)
        val triggering = geoEvent.triggeringGeofences

        val notificationManager = ContextCompat.getSystemService(context, NotificationManager::class.java)

        for (geo in triggering) {
            // Log info about activating a geofence
            Log.i("geofence", "Geofence z id: ${geo.requestId} aktywny.")

            // Display notification depending on action
            when (geoEvent.geofenceTransition) {
                Geofence.GEOFENCE_TRANSITION_ENTER -> {
                    notificationManager?.sendGeofenceNotification(context, generateGreeting(geo.requestId),  generateDiscount())
                    Log.i("geofences", "Wejście: ${geoEvent.triggeringLocation}")
                }
                Geofence.GEOFENCE_TRANSITION_EXIT -> {
                    notificationManager?.sendGeofenceNotification(context, generateGoodbye(geo.requestId),  HAVE_A_NICE_DAY)
                    Log.i("geofences", "Wyjście: ${geoEvent.triggeringLocation}")
                }
                else -> {
                    Log.e("geofences", "Błąd w aplikacji.")
                }
            }
        }
    }

    private fun generateDiscount() : String {
        val items = listOf("spodnie", "buty", "koszule", "sukienki", "artykuły spożywcze", "kometyki", "gry planszowe", "biżuterię","akcesoria")
        val discount = (Math.random() * 8).roundToInt() * 10 + 10
        return "Dzisiejsza promocja dnia to rabat $discount% na ${items.random()}."
    }

    private fun generateGoodbye(requestId: String) : String {
        return "Właśnie opusciłeś obszar \"${requestId.split(DELIMITER)[1]}\"..."
    }

    private fun generateGreeting(requestId: String) : String {
        return "Witaj w \"${requestId.split(DELIMITER)[1]}\"!!!"
    }

    companion object {
        const val DELIMITER = " - "
        const val HAVE_A_NICE_DAY = "Miłego dnia, do zobaczenia!"
    }
}
