package pl.edu.pjatk.smb.mdziurzynski.smb1.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import pl.edu.pjatk.smb.mdziurzynski.smb1.dao.LocationDao
import pl.edu.pjatk.smb.mdziurzynski.smb1.dao.ProductDao
import pl.edu.pjatk.smb.mdziurzynski.smb1.model.Location
import pl.edu.pjatk.smb.mdziurzynski.smb1.model.Product

@Database(entities = [Product::class, Location::class], version = 2)
abstract class DatabaseHandler : RoomDatabase() {

    abstract fun productDao(): ProductDao
    abstract fun locationDao(): LocationDao

    // singleton
    companion object {
        private var instance: DatabaseHandler? = null

        fun getDatabase(context: Context): DatabaseHandler{
            val temporaryInstance = instance
            if (temporaryInstance != null) {
                return temporaryInstance
            }

            val newInstance = Room.databaseBuilder(
                context.applicationContext,
                DatabaseHandler::class.java,
                "product_database"
            ).allowMainThreadQueries()
                .fallbackToDestructiveMigration()
                .build()
            instance = newInstance
            return newInstance
        }
    }
}