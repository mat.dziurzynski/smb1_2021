package pl.edu.pjatk.smb.mdziurzynski.smb1.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import pl.edu.pjatk.smb.mdziurzynski.smb1.model.Location

@Dao
interface LocationDao {
    @Query(QUERY_GET_PRODUCTS)
    fun getLocations(): LiveData<List<Location>>

    @Insert
    fun insert(location: Location)

    @Update
    fun update(location: Location)

    @Delete
    fun delete(location: Location)

    @Query(QUERY_DELETE_BY_ID)
    fun deleteById(foundId: Long)

    @Query(QUERY_DELETE_PRODUCTS)
    fun deleteLocations()

    companion object {
        private const val QUERY_GET_PRODUCTS = "SELECT * FROM location_table;"
        private const val QUERY_DELETE_BY_ID = "DELETE FROM location_table WHERE location_table.id = :foundId"
        private const val QUERY_DELETE_PRODUCTS = "DELETE FROM location_table;"
    }
}